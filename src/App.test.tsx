import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders app version', () => {
  render(<App />);

  expect(screen.getByText(/app version:/i)).toBeInTheDocument();
});
