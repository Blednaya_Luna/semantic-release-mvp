## [2.1.0](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/compare/v2.0.0...v2.1.0) (2021-10-13)


### Features

* feat test commit ([8277465](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/8277465f6f16a6371cbd490a29688c82406e31df))
* **TASK-1031:** feat test commit with task ([cb744ff](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/cb744ffb7bb0adf2d2e3b17f7e39cc0490297c20))


### Bug Fixes

* fix test commit ([bc65a31](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/bc65a31320e2d46fafb08000959267e8488ec19f))
* **TASK-1032:** fix test commit with task ([3d01b85](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/3d01b8571d0372a43e1f55d5b22c66f73e30f7ca))


### Documentation

* docs test commit ([90999ef](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/90999efd023cd33b77efdbba0817053886ad735e))
* **TASK-1034:** docs test commit with task ([556e4df](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/556e4df65633fb48fa93f26946fe874849d0a61d))


### Style changes

* style test commit ([c4f5634](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/c4f5634d7cb05dcac4021c8d577a01a3cbc787f6))
* **TASK-1055:** style test commit with task ([f183e65](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/f183e65b49ed6bf029014f571b1e8dbdbcc47a7c))


### Refactor

* refactor test commit ([4dee676](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/4dee6762db62a0dfd97da1086e87443e5527e285))
* **TASK-1118:** refactor test commit with task ([51867e2](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/51867e230576596e68d3bda541063b7f0ac8014a))


### Performance optimization

* perf test commit ([fc9ba67](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/fc9ba67dd08b098239895f0d75113af824043774))
* **TASK-1119:** perf test commit with task ([d869a83](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/d869a8306ce8b62a41ef7f2adb75a827768a043f))


### CI/CD

* ci test commit ([5d0adbd](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/5d0adbda136da1a908b66a0086a18f161bdd9e6c))
* **TASK-1221:** ci test commit with task ([b232178](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/b2321781f7a392a02ba9f6d6bf58e356120c4069))


### Test changes

* build test commit ([82bea1b](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/82bea1bc57b27db7d80c7519d158fec583d3d433))
* **TASK-1033:** build test commit with task ([cf7af91](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/cf7af91766c755f5b4886fda8683dc42f45eeea5))
* **TASK-1225:** test test commit with task ([9597628](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/959762821c22cfc91e4451b99f6fdde831014990))
* test test commit ([f99d189](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/f99d189eeaec57fb7e2fe946ba25395c7080f127))

## [2.0.0](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/compare/v1.0.0...v2.0.0) (2021-10-13)


### ⚠ BREAKING CHANGES

* some breaking change

### Features

* breaking change test commit ([f3a2a53](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/f3a2a531706232d65ef811f00e7b6a84b9e9565e))

## 1.0.0 (2021-10-13)


### Features

* display app version ([d9bb58e](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/d9bb58ed0c76b67f3b93d4df8f245a309a7303b0))
* setup semantic-release ([b407ec0](https://gitlab.com/Blednaya_Luna/semantic-release-mvp/commit/b407ec08a04bb10cd793bb5c63ead6cf4fd28795))
